import cv2
import numpy as np
import streamlit as st
import tensorflow as tf
from PIL import Image
from CAM import GradCAM

@st.cache(allow_output_mutation=True)
def get_model():
    return tf.keras.models.load_model('model.h5')

st.markdown(f'<style>{open("style.css").read()}</style>', unsafe_allow_html=True)

st.title('42028: Deep Learning and Convolutional Neural Network (Autumn 2020)')
st.header('Respiratory Disease Detection')
st.subheader('Group 42')
st.write('')
st.write('')

upload_box = st.empty()
label = st.empty()
image = st.empty()
progress = st.empty()
detect = st.empty()
st.write('')
st.write('')
st.write('')
st.write('')
plot = st.empty()

upload = upload_box.file_uploader('', type=['png', 'jpg'])

if upload is not None:

    img = Image.open(upload)

    image.image(img, width=500)

    if detect.button('Detect'):

        with st.spinner('Processing...'):

            img = np.array(img)

            if len(img.shape) > 2 and img.shape[2] == 4:
                img = cv2.cvtColor(img, cv2.COLOR_BGRA2BGR)
                final_map = cv2.COLOR_BGR2RGBA
            else:
                final_map = cv2.COLOR_BGR2RGB

            x = cv2.resize(img, (299, 299))
            x = x / 255.0
            x = np.expand_dims(x, axis=0)

            model = get_model()

            output = model.predict(x)
            prediction = np.argmax(output)
            accuracy = output[0][prediction]

            cam = GradCAM(model=model, classIdx=prediction, layerName='block14_sepconv2_act')
            heatmap = cam.compute_heatmap(x)

            heatmap = cv2.resize(heatmap, (img.shape[1], img.shape[0]))
            heatmap = cv2.applyColorMap(heatmap, cv2.COLORMAP_HOT)  # COLORMAP_JET, COLORMAP_VIRIDIS, COLORMAP_HOT

            result = cv2.addWeighted(heatmap, 0.5, img, 1.0, 0)

            result = Image.fromarray(cv2.cvtColor(result, final_map))
            image.image(result, width=500)

        classes = {0:'COVID-19', 1: 'HEALTHY', 2: 'PNEUMONIA/OTHER'}
        color = {0:'red', 1: 'green', 2: 'orange'}
        label.markdown(f'<font size="3" color="{color[prediction]}">{classes[prediction]}</font>', unsafe_allow_html=True)
        confidence = progress.progress(0)
        confidence.progress(np.ceil(accuracy*100)/100)